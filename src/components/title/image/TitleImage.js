import { Component } from "react";
import background from "../../../assets/images/background.jpg";

class TitleImage extends Component {
    render() {
        return (
            <img className="img-thumbnail" src={background} alt="title background"/>
        )
    }
}

export default TitleImage;